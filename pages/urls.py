from django.urls import path, include
from . import views
from adminapp.views import dashboard

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home),
    path('login/', views.loginApp, name='login'),
    path('achievements/',views.achievements, name='achievements'),
    path('gallery/',views.gallery, name='gallery'),
    path('about/',views.contact_us, name='contact_us'),
    path('adminapp/', dashboard, name='adminapp'),
    path('logout/', views.logoutApp, name='logout')
    
]
