from django.db import models

class Member(models.Model):
    ROLE = (
        ('Member','Member'),
        ('Administrator','Administrator')
    )
    fullname = models.CharField(max_length=50, null=True)
    username = models.CharField(max_length=50, null=True)
    phone = models.CharField(max_length=10, null=True)
    email = models.EmailField(max_length=254, null=True)
    password = models.CharField(max_length=50, null=True)
    role = models.CharField(max_length=50, null=True, choices=ROLE)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    github_username = models.CharField(max_length=50, null=True)
    discord_handle = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.fullname


class Achievements(models.Model):

    title = models.CharField(max_length=200, null=True)
    content = models.CharField(max_length=1000, null=True)
    achievers = models.CharField(max_length=500, null=True)
    image = models.ImageField(null=True)
    date = models.DateField(null=True)
    post_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.title
    
